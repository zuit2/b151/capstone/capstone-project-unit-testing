const chai = require("chai");
// const expect = chai.expect;
const { expect } = require("chai");
const http = require("chai-http");

chai.use(http);

const url = "http://localhost:5001";

describe("Capstone Test Suite", () => {
  it("Check if Post /currency is running", () => {
    chai
      .request(url)
      .post("/currency")
      .type("json")
      .send({
        alias: "sar",
        name: "Saudi Arabian Riyal",
        ex: {
          peso: 13.43,
          usd: 0.27,
          yen: 30.33,
          yuan: 1.7,
        },
      })
      .end((err, res) => {
        // console.log(res.status);
        expect(res.status).to.not.equal(undefined);
      });
  });
  it("Check if Post /currency returns status 400 if name is missing", (done) => {
    chai
      .request(url)
      .post("/currency")
      .type("json")
      .send({
        alias: "sar",
        ex: {
          peso: 13.43,
          usd: 0.27,
          yen: 30.33,
          yuan: 1.7,
        },
      })
      .end((err, res) => {
        // console.log(res.status);
        expect(res.status).to.equal(400);
        done();
      });
  });
  it("Check if Post /currency returns status 400 if name is not a string", (done) => {
    chai
      .request(url)
      .post("/currency")
      .type("json")
      .send({
        alias: "sar",
        name: true,
        ex: {
          peso: 13.43,
          usd: 0.27,
          yen: 30.33,
          yuan: 1.7,
        },
      })
      .end((err, res) => {
        // console.log(res.status);
        expect(res.status).to.equal(400);
        done();
      });
  });
  it("Check if Post /currency returns status 400 if name is an empty string", (done) => {
    chai
      .request(url)
      .post("/currency")
      .type("json")
      .send({
        alias: "sar",
        name: "",
        ex: {
          peso: 13.43,
          usd: 0.27,
          yen: 30.33,
          yuan: 1.7,
        },
      })
      .end((err, res) => {
        // console.log(res.status);
        expect(res.status).to.equal(400);
        done();
      });
  });
  it("Check if Post /currency returns status 400 if ex is missing", (done) => {
    chai
      .request(url)
      .post("/currency")
      .type("json")
      .send({
        alias: "sar",
        name: "Saudi Arabian Riyal",
      })
      .end((err, res) => {
        // console.log(res.status);
        expect(res.status).to.equal(400);
        done();
      });
  });
  it("Check if Post /currency returns status 400 if ex is not an object", (done) => {
    chai
      .request(url)
      .post("/currency")
      .type("json")
      .send({
        alias: "sar",
        name: "Saudi Arabian Riyal",
        ex: true,
      })
      .end((err, res) => {
        // console.log(res.status);
        expect(res.status).to.equal(400);
        done();
      });
  });
  it("Check if Post /currency returns status 400 if ex is an empty object", (done) => {
    chai
      .request(url)
      .post("/currency")
      .type("json")
      .send({
        alias: "sar",
        name: "Saudi Arabian Riyal",
        ex: {},
      })
      .end((err, res) => {
        // console.log(res.status);
        expect(res.status).to.equal(400);
        done();
      });
  });
  it("Check if Post /currency returns status 400 if alias is missing", (done) => {
    chai
      .request(url)
      .post("/currency")
      .type("json")
      .send({
        name: "Saudi Arabian Riyal",
        ex: {
          peso: 13.43,
          usd: 0.27,
          yen: 30.33,
          yuan: 1.7,
        },
      })
      .end((err, res) => {
        // console.log(res.status);
        expect(res.status).to.equal(400);
        done();
      });
  });
  it("Check if Post /currency returns status 400 if alias is not a string", (done) => {
    chai
      .request(url)
      .post("/currency")
      .type("json")
      .send({
        alias: true,
        name: "Saudi Arabian Riyal",
        ex: {
          peso: 13.43,
          usd: 0.27,
          yen: 30.33,
          yuan: 1.7,
        },
      })
      .end((err, res) => {
        // console.log(res.status);
        expect(res.status).to.equal(400);
        done();
      });
  });
  it("Check if Post /currency returns status 400 if alias is an empty string", (done) => {
    chai
      .request(url)
      .post("/currency")
      .type("json")
      .send({
        alias: "",
        name: "Saudi Arabian Riyal",
        ex: {
          peso: 13.43,
          usd: 0.27,
          yen: 30.33,
          yuan: 1.7,
        },
      })
      .end((err, res) => {
        // console.log(res.status);
        expect(res.status).to.equal(400);
        done();
      });
  });
  it("Check if Post /currency returns status 400 if all fields are complete but there is a duplicate alias", (done) => {
    chai
      .request(url)
      .post("/currency")
      .type("json")
      .send({
        alias: "yuan",
        name: "Saudi Arabian Riyal",
        ex: {
          peso: 13.43,
          usd: 0.27,
          yen: 30.33,
          yuan: 1.7,
        },
      })
      .end((err, res) => {
        // console.log(res.status);
        expect(res.status).to.equal(400);
        done();
      });
  });
  it("Check if Post /currency returns status 200 if all fields are complete and there is no duplicate", (done) => {
    chai
      .request(url)
      .post("/currency")
      .type("json")
      .send({
        alias: "sar",
        name: "Saudi Arabian Riyal",
        ex: {
          peso: 13.43,
          usd: 0.27,
          yen: 30.33,
          yuan: 1.7,
        },
      })
      .end((err, res) => {
        // console.log(res.status);
        expect(res.status).to.equal(200);
        done();
      });
  });
});
