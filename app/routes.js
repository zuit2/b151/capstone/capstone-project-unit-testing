const { exchangeRates } = require("../src/util.js");

module.exports = (app) => {
  app.get("/currency", (req, res) => {
    return res.send({
      currencies: exchangeRates,
    });
  });

  app.post("/currency", (req, res) => {
    if (!req.body.hasOwnProperty("name")) {
      return res.status(400).send({
        error: "Bad Request : missing required parameter NAME",
      });
    }
    if (typeof req.body.name !== "string") {
      return res.status(400).send({
        error: "Bad Request : NAME has to be a string",
      });
    }
    // console.log(`-name length: ${req.body.name.length}`);
    if (req.body.name.length === 0) {
      return res.status(400).send({
        error: "Bad Request : NAME should not be an empty string",
      });
    }
    if (!req.body.hasOwnProperty("ex")) {
      return res.status(400).send({
        error: "Bad Request : missing required parameter EX",
      });
    }
    if (typeof req.body.ex !== "object") {
      return res.status(400).send({
        error: "Bad Request : EX has to be an object",
      });
    }
    // console.log(`-ex length: ${Object.keys(req.body.ex).length}`);
    if (Object.keys(req.body.ex).length === 0) {
      return res.status(400).send({
        error: "Bad Request : EX should not be an empty object",
      });
    }
    if (!req.body.hasOwnProperty("alias")) {
      return res.status(400).send({
        error: "Bad Request : missing required parameter ALIAS",
      });
    }
    if (typeof req.body.alias !== "string") {
      return res.status(400).send({
        error: "Bad Request : ALIAS has to be a string",
      });
    }
    if (req.body.alias.length === 0) {
      return res.status(400).send({
        error: "Bad Request : ALIAS should not be an empty string",
      });
    }
    // console.log(`-alias: ${req.body.alias}`);
    if (
      req.body.hasOwnProperty("alias") &&
      req.body.hasOwnProperty("name") &&
      req.body.hasOwnProperty("ex")
    ) {
      Object.values(exchangeRates).forEach((val) => {
        // console.log(val.alias);
        if (val.alias === req.body.alias) {
          return res.status(400).send({
            error:
              "Bad Request : fields are complete but ALIAS should not be duplicate",
          });
        }
      });
      return res.status(200).send({
        message: "Success : fields are complete and there is no duplicate",
      });
    }
    else return res.status(400).send({
      error:
        "Bad Request : fields are incomplete",
    });
  });
};
